from flask import Flask, request, jsonify
from flask_restful import Resource, Api
import os
# import time
import openai
import cv2
import pytesseract
from pytesseract import Output


# from pdf2image import convert_from_path
# from PIL import Image
# import numpy as np
# from ExtractScannedPDFText import extract_ocr_text
from dotenv import load_dotenv


load_dotenv()

# Set your OpenAI API key
openai.api_key = "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"

app = Flask(__name__)
api = Api(app)


def extract_sentences_from_data(data):
    sentences = []
    lefts = []
    widths = []

    current_sentence = []
    current_left = []
    current_width = []
    
    for word, left, width in zip(data['text'], data['left'], data['width']):
        # If the word is not empty or whitespace, append it to the current_sentence
        if word.strip():
            current_sentence.append(word)
            current_left.append(left)
            current_width.append(width)
        else:
            # If we find an empty word and the current sentence is not empty, we consider the sentence complete
            if current_sentence:
                sentences.append(current_sentence)
                lefts.append(current_left)
                widths.append(current_width)
                current_sentence = []
                current_left = []
                current_width = []

    # Check for any remaining sentence after the loop
    if current_sentence:
        sentences.append(current_sentence)
        lefts.append(current_left)
        widths.append(current_width)

    return sentences, lefts, widths

def write_sentences_to_file(sentences, lefts, widths, filename):
    """
    Writes a list of sentences to a file, maintaining specific positions and widths for words on the same line.
    Each sentence starts from the beginning of a new line.
    """
    with open(filename, 'w') as file:
        for sentence, left, width in zip(sentences, lefts, widths):
            line = ''
            for i, (word, l, w) in enumerate(zip(sentence, left, width)):
                if i == 0:
                    # For the first word, its start position is exactly at 'l'
                    line += ' ' * (l // 10) # Add spaces to reach the 'left' position of the first word
                else:
                    # Calculate the gap between the current word and the previous word
                    gap = l - (left[i - 1] + width[i - 1])
                    
                    # If the gap is 10 or less, we set it to 1
                    if gap < 10:
                        gap = 1
                    else:
                        gap = gap // 10
                        
                    line += ' ' * gap  # Add the calculated gap (in spaces) before the word
                
                line += word  # Add the word itself
            
            file.write(line + '\n')
            
    # Read the content of a file
    with open(filename, 'r') as file:
        content = file.read()

    # print(content)
    return content

def ocr_images(input_file, out_put_text_file):
    """
    Perform Optical Character Recognition (OCR) on an image and write the recognized text to a file.
    
    This function reads an image file, performs OCR using Pytesseract, extracts sentences,
    and writes the recognized text to an output text file while attempting to maintain
    the spatial alignment of the text as it appears in the original image.
    """
    
    img = cv2.imread(input_file)
    data = pytesseract.image_to_data(img, output_type=Output.DICT)
    print("==========================dataaa", data)
    sentences, lefts, widths = extract_sentences_from_data(data)
    data = write_sentences_to_file(sentences, lefts, widths, out_put_text_file)
    return data


# Function to generate a response using OpenAI Chat API
def generate_response(conversation_history, user_message):
    messages = conversation_history + [{"role": "user", "content": user_message}]
    response = openai.ChatCompletion.create(
        model="gpt-3.5-turbo",
        messages=messages,
    )
    generated_text = response['choices'][0]['message']['content']

    return generated_text

class ExtractFileData(Resource):
    def post(self):
        response = None  # Initialize response with a default value
        try:
            # Get the file from the request (assuming it's a PDF or image file)
            file = request.files['file']
            # Create a folder 'pdf_storage' in the same directory where your script is
            if not os.path.exists('pdf_storage'):
                os.makedirs('pdf_storage')
            file_path = "pdf_storage/" + file.filename
            file.save(file_path)


            # Perform OCR on the file
            ocr_text = ocr_images(file_path, 'output_text/final/output.txt')
            # ocr_text = extract_ocr_text(file_path, "test_out_folder_api" , "7", "3", "7")
            print(ocr_text)

            # Initialize the conversation history with the OCR data as the initial user message
            conversation_history = [{"role": "system", "content": "You are a helpful assistant."},
                                    {"role": "user", "content": ocr_text}]

            # Get the user prompt from the request
            user_prompt = request.form['prompt']

            # Generate a response using OpenAI Chat API
            response = generate_response(conversation_history, user_prompt)
        except Exception as e:
            print("Error generating", e)

        # Delete the file after processing
        # os.remove(file_path)

        return jsonify({"response": response})

api.add_resource(ExtractFileData, '/extract_data')


if __name__ == '__main__':
    app.run(debug=True)
