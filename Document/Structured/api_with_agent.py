import requests
import openai
import json
import os 
import re
import csv
import ast
import fitz  # PyMuPDF
import io
from PIL import Image
import pandas as pd
from pathlib import Path


def download_pdf(link):

    file_id = link.split('/file/d/')[1].split('/view')[0]
    # The API endpoint
    url = 'http://localhost:3000/api-n/drive/download-file'
    # The data to be sent with the POST request
    params = {'fileId': file_id}
    try:
        # Make a POST request to the API
        response = requests.post(url, params=params)
        if response.status_code == 200:
            response_json = response.json()
            file_path = response_json.get("file_path")
                       
            return file_path
            # Here you can handle the response content if needed
            # For example, to save the file:
            # with open('downloaded_file', 'wb') as f:
            #     f.write(response.content)
        else:
            print(f'Failed to download file. Status code: {response.status_code}')
            return None 
    except requests.exceptions.RequestException as e:
        # Handle any errors that occur during the request
        print(f'An error occurred: {e}')
        


def convert_pdf_to_grayscale_images(pdf_path):
    # # Check if output folder exists, if not create it
    # Extract the directory from the file path
    folder_path = Path(pdf_path).parent

    # Define the new folder path for 'images'
    output_folder = folder_path / 'images' 
    
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    pdf_document = fitz.open(pdf_path)
    page_count = pdf_document.page_count
    
    if page_count > 10:
        # print("The PDF has more than 10 pages. Limiting the processing.")
        return False
    for page_number in range(pdf_document.page_count):
        page = pdf_document[page_number]
        image_list = page.get_images(full=True)

        for img_index, img in enumerate(image_list):
            xref = img[0]
            base_image = pdf_document.extract_image(xref)
            image_data = base_image["image"]

            # Create a PIL Image from the image data
            image = Image.open(io.BytesIO(image_data))
            image_extension = base_image["ext"]

            # Convert the image to grayscale
            gray_img = image.convert('L')

            # Save the grayscale image with a unique name to the output folder
            image_name = f"page_{page_number + 1}_image_{img_index + 1}_gray.{image_extension}"
            gray_img.save(f"{output_folder}/{image_name}")
            print(f"Saved grayscale image: {image_name}")

    # Close the PDF file
    pdf_document.close()
    return True, output_folder
    
    
# -------------------------------------------------------------------------
def save_json_to_excel(input_string, output_file):
    try:
        
        # Splitting the data into key-value pairs based on the newline and colon characters
        key_value_pairs = [item.split(": ") for item in input_string['response'].split('\n')]

        # Create a DataFrame
        df = pd.DataFrame(key_value_pairs, columns=['Attribute', 'Value'])

        # Save the DataFrame to an Excel file
        df.to_excel(output_file, index=False, engine='openpyxl')
        
     
    except (ValueError, SyntaxError) as e:
        print(f"An error occurred: {str(e)}")
         

# -----------------------------------------------------------------------------

def process_pdf_from_url(pdf_url, prompt):
    
   
    local_path = download_pdf(pdf_url)
    
    folder_path = Path(local_path).parent

    # Define the new folder path for 'images'
    output_folder = folder_path / 'Structured_Result' 
    
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)
    name = Path(local_path).stem
    
    excel_filename = f"{output_folder}/{name}.xlsx"
    if local_path == None: 
        return None 
    else:
    
        counter_boolean, output_images_folder = convert_pdf_to_grayscale_images(local_path)
        
        if counter_boolean:
            url = "http://localhost:5000/extract_data"

            for filename in os.listdir(output_images_folder):
                # Check if the file is an image (you might adjust this check according to your needs)
                if filename.lower().endswith(('.png', '.jpg', '.jpeg')):
                    image_path = os.path.join(output_images_folder, filename)
                    print("===============image path", image_path)
                    # Process the image
                    # image_name = os.path.splitext(os.path.basename(image_path))[0]

            
                    # Ensure the file exists before sending the request
                    try:
                        files = {'file': open(image_path, 'rb')}
                    except FileNotFoundError:
                        print(f"No such file: '{image_path}'")
                        return None
            
                    # Defining the payload
                    data = {'prompt': prompt}
                    
                    # Making the API request and returning the response
                    try:
                        response = requests.post(url, files=files, data=data)
                        # print("=======response", response.text)
                        # return response
                        
                        json_object = json.loads(response.text)
                        # print("============json object", json_object)
                        save_json_to_excel(json_object, excel_filename)
                    except requests.RequestException as e:
                        print(f"An error occurred: {str(e)}")
                        return None
                    finally:
                        # Closing the file
                        files['file'].close()
            return json_object

        else:
            return "The PDF has more than 10 pages. Limiting the processing."

