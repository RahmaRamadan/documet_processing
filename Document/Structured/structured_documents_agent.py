from typing import Type
from pydantic import BaseModel, Field
from langchain.tools import BaseTool
from langchain.agents import AgentType,initialize_agent
from langchain.chat_models import ChatOpenAI
from api_with_agent import process_pdf_from_url
from dotenv import load_dotenv

load_dotenv()


class ProcessPdfInput(BaseModel):
    """Inputs for process_pdf_from_url"""

    pdf_url: str = Field(description="The URL where the PDF is located")
    prompt: str = Field(description="the full objective required to extract information, it is the whole prompt without URL")

class PdfProcessingTool(BaseTool):
    name = "process_pdf_from_url"
    description = """
    - Process a PDF from a given URL by converting it to images and then sending 
    - each image to a specified API endpoint along with an additional prompt.
    - must extract the details or the information according to the user input only.
    - Only extract fields that are present.
    - remove fields that is Null, empty, or not specified.
    - Mustn't generate any fields with no data or values.
    - Don't create data or fields that is not provided or mentioned in the document.
    - output will be in a json format 
    """
    args_schema: Type[BaseModel] = ProcessPdfInput

    def _run(self, pdf_url: str, prompt: str):
        response = process_pdf_from_url(pdf_url, prompt)
        print("function response", response)
        return response

    def _arun(self, pdf_url: str, prompt: str):
        raise NotImplementedError("process_pdf_from_url does not support async")

# Set up the turbo LLM
turbo_llm = ChatOpenAI(
    temperature=0,
    model_name='gpt-3.5-turbo-1106'
)

tools = [PdfProcessingTool()]
# create our agent
prompt = """
    - Pass a PDF from a given URL
    - Just stick to the fields in the provided prompt and don't extract any extra fields
    - Don't extract line items details 
    - Only extract fields that are present.
    - Must avoid capturing any field that doesn't exist in the invoice and not specified.
    User: {input}
"""
agent = initialize_agent(
    tools=tools,
    llm=turbo_llm,
    prompt=prompt,
    agent=AgentType.OPENAI_MULTI_FUNCTIONS,
    verbose=True,
    max_iterations=3,
)
# # agent.run("create a word file  about AI and add a sections for it with bullets point add descriptive add at least 5 sections")
# agent.run("Please extract the following fields from this URL 'https://drive.google.com/file/d/14ofQ0luqrvrBgY36EVzHqy4qCLjBCaSK/view?usp=sharing' if they exist only: invoice no., invoice date, bill to name, bill to address, ship to name, ship to address, sub total, discount, total tax, total amount, email, phone number. Please don't extract the fields that are not exist in the text data. Please provide the response in json format. please use the response from the api")
# agent.run("Extract, solely if present, the following fields from the text data accessed via this URL 'https://drive.google.com/file/d/14ofQ0luqrvrBgY36EVzHqy4qCLjBCaSK/view?usp=sharing': invoice no., invoice date, bill to name, bill to address, ship to name, ship to address, sub total, discount, total tax, total amount, email, and phone number, ensuring to omit non-existent fields and to format the final response in JSON, utilizing API-derived responses.")
# agent.run("please provide the response in a json format based on the following url `https://drive.google.com/file/d/14ofQ0luqrvrBgY36EVzHqy4qCLjBCaSK/view?usp=sharing`, extract the following fileds,`invoice no., invoice date, bill to name, bill to address, ship to name, ship to address, sub total, discount, total tax, total amount, email, and phone number`"
#           "don't generate any random prompts")
# agent.run("find the stock market of meta")

agent.run("from the following URL `https://drive.google.com/file/d/14ofQ0luqrvrBgY36EVzHqy4qCLjBCaSK/view?usp=sharing`, For the invoice at hand, diligently extract: Invoice Number (potentially labeled as Invoice ID, Bill Number, Receipt Number), discern the Invoice Date (alternatively called Date Issued, Bill Date) and the Due Date (or Pay By Date, Payment Due Date), identify the Vendor Name (sometimes referred to as Seller, Shipped Name, Billed By, Merchant) and Address (possibly denoted as Vendor Address, Shipped Address, Seller Address, Vendor Location), similarly extract Customer Name (alternatively Buyer, Billed To, Client) and Address (which could be Buyer Address, Shipping Address, Delivery Address), delve into Payment Amounts such as Total Amount (occasionally Grand Total, Total Due, Amount Payable), Taxes (look for Tax Amount, VAT, Sales Tax, GST), and Discounts (perhaps marked as Discount, Deduction, Rebate). Further, decode Payment details like Terms (also called Terms and Conditions, Payment Conditions) and Method (or Mode of Payment, Payment Type), uncover Bank Details (like Bank Account Number, Bank Name, IBAN, SWIFT/BIC Code), spot the Purchase Order Number (often PO Number, Order ID), determine Shipping Charges (sometimes Shipping, Delivery Fees, Freight Charges), identify the Currency (or Currency Code, Payment Currency), and finally, don't overlook any Remarks or Notes (which may be under Comments, Additional Information and remove line items details from the result.")
# agent.run("Access the invoice via the provided URL https://drive.google.com/file/d/14ofQ0luqrvrBgY36EVzHqy4qCLjBCaSK/view?usp=sharing and carefully extract critical details such as the Invoice Number—which could also be under various labels like Invoice ID, Bill Number, or Receipt Number—and pinpoint the Invoice Date and Due Date, which might be listed as Date Issued, Bill Date, Pay By Date, or Payment Due Date. Identify the Vendor Name and Address, which could be under Seller, Shipped Name, Billed By, or Merchant, and Vendor Address, Shipped Address, Seller Address, or Vendor Location, respectively. Also, pull the Customer Name and Address, recognizing they might be termed Buyer, Billed To, Client, or associated with addresses such as Buyer Address, Shipping Address, or Delivery Address. Analyze Payment Amounts, including the Total Amount, Taxes, and Discounts, which may appear as Grand Total, Total Due, Amount Payable, Tax Amount, VAT, Sales Tax, GST, Discount, Deduction, or Rebate. Decode Payment details like Terms and Method, which could be referred to as Terms and Conditions, Payment Conditions, Mode of Payment, or Payment Type, and reveal Bank Details, including Bank Account Number, Bank Name, IBAN, or SWIFT/BIC Code. Locate the Purchase Order Number, listed perhaps as PO Number or Order ID, assess Shipping Charges that might be described as Shipping, Delivery Fees, or Freight Charges, discern the Currency used, and finally, ensure to note any Remarks or Notes that might be included under Comments or Additional Information.")

# agent.run("from the following URL `https://drive.google.com/file/d/14ofQ0luqrvrBgY36EVzHqy4qCLjBCaSK/view?usp=sharing`")