from langchain.agents import AgentType,initialize_agent
from pdf_functionalities import query_pdf_file
from langchain.chat_models import ChatOpenAI
from pydantic import BaseModel, Field
from langchain.tools import BaseTool
from typing import Type
from dotenv import load_dotenv

load_dotenv()

class QueryPdfInput(BaseModel):
    """Inputs for query_pdf_file"""

    URL: str = Field(description="The web address or path where the Pdf document resides")
    query: str = Field(description="The search string or keyword for which relevant content from the Pdf file is to be retrieved")

class PdfProcessingTool(BaseTool):
    name = "query_pdf_file"
    description = """
    Download a Pdf document from a specified URL, then scan it for content matching the given query, displaying the findings in a json format.
    """
    args_schema: Type[BaseModel] = QueryPdfInput

    def _run(self, URL: str, query: str):
        response = query_pdf_file(URL, query)
        print("function response", response)
        return response

    def _arun(self, URL: str, query: str):
        raise NotImplementedError("query_pdf_file does not support async")

# Set up the turbo LLM
turbo_llm = ChatOpenAI(
    temperature=0,
    model_name='gpt-3.5-turbo'
)

tools = [PdfProcessingTool()]
# create our agent
agent = initialize_agent(
    tools=tools,
    llm=turbo_llm,
    agent=AgentType.OPENAI_MULTI_FUNCTIONS,
    verbose=True,
    max_iterations=3,
)

agent_output = agent.run("from the following URL `https://drive.google.com/file/d/1D5a6PnqTzzOesM7aF_CIIK_J-N_MVH2l/view?usp=sharing`, please tell me only the course and certification?")
print(agent_output)