from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain.document_loaders import PyPDFLoader
from langchain.embeddings import OpenAIEmbeddings
from langchain.prompts import PromptTemplate
from langchain.chat_models import ChatOpenAI
from langchain.vectorstores import Chroma
from langchain.chains import LLMChain
from dotenv import load_dotenv
import requests
import fitz

load_dotenv()

def download_pdf_file_from_google_drive(link):
    file_id = link.split('/file/d/')[1].split('/view')[0]
    # The API endpoint
    url = 'http://localhost:3000/api-n/drive/download-file'
    # The data to be sent with the POST request
    params = {'fileId': file_id}

    try:
        # Make a POST request to the API
        response = requests.post(url, params=params)
        if response.status_code == 200:
            response_json = response.json()
            file_path = response_json.get("file_path")
            pdf_document = fitz.open(file_path)
            page_count = pdf_document.page_count
            
            if page_count > 11:
            # Check if the request was successful
                return False, False
            else:
                print('File downloaded successfully')
                print(response)
                return True, file_path
            
        else:
            print(f'Failed to download file. Status code: {response.status_code}')
            return False, None 
    
    except requests.exceptions.RequestException as e:
        # Handle any errors that occur during the request
        print(f'An error occurred: {e}')

             
def pdf_file_searching(file_path, query):
    # Load the Pdf file
    # limit the pdf if the pages more than 10 pages 
    
    # Create a custom agent using the ChatOpenAI model
    llm = ChatOpenAI(model_name="gpt-3.5-turbo", temperature=0)

    loader = PyPDFLoader(file_path)
    document = loader.load()
    
    # Split the document into chunks
    text_splitter = RecursiveCharacterTextSplitter(chunk_size=500, chunk_overlap=0)

    chunks = text_splitter.split_documents(document)
    prompt = PromptTemplate.from_template(
        "ُExtract the query as it is and search with it in the following and get the result in bullet points: {docs}"
    )
    # Chain
    llm_chain = LLMChain(llm=llm, prompt=prompt)

    # Embed and store the chunks
    embeddings = OpenAIEmbeddings()
    vectorstore = Chroma.from_documents(documents=chunks, embedding=embeddings)
  
    docs = vectorstore.similarity_search(query)
    print(len(docs))
    
    # Perform the query
    result = llm_chain(docs)
    return result["text"] 

def query_pdf_file(url, query_prompt):
    try:

        counter_boolean, local_path = download_pdf_file_from_google_drive(url)
        if local_path == None:
            return None 
        if counter_boolean:
            response = pdf_file_searching(local_path, query_prompt)
            return response
        
        else:
            return "The PDF has more than 10 pages. Limiting the processing."

            
    except Exception as e:
        print("error", e)
        

# Example usage
# file_path = "https://drive.google.com/file/d/159qTxRikcSwFyqQOoEalw5vG2yl3hNyn/view?usp=sharing"
# query = 'tell me only the personal information'

# result = query_pdf_file(file_path, query)
# print(result)