from csv_functionalities import query_csv_file, query_csv_file_with_prompt
from langchain.agents import AgentType,initialize_agent
from langchain.chat_models import ChatOpenAI
from pydantic import BaseModel, Field
from langchain.tools import BaseTool
from dotenv import load_dotenv
from typing import Type

load_dotenv()

class QueryCSVInput(BaseModel):
    """Inputs for query_csv_file"""

    URL: str = Field(description="The web address or path where the CSV document resides")

class PromptCSVInput(BaseModel):
    """Inputs for query_csv_file_with_prompt"""

    prompt: str = Field(description="The provided string specifies a text-based search string, designed to retrieve relevant information or content based on keyword matches. This string is meant for a text processing prompt and is not related to database or SQL operations. Users should structure their input in a clear and concise manner to ensure optimal results")


class CSVProcessingTool(BaseTool):
    name = "query_csv_file"
    # description = """
    # Download a CSV document from a specified URL, then scan it for content matching the given prompt, displaying the findings in a json format.
    # """
    description = """
    This function downloads an CSV file from Google Drive and save it in a local folder
    """
    args_schema: Type[BaseModel] = QueryCSVInput

    def _run(self, URL: str ):
        response = query_csv_file(URL)
        print("function response", response)
        return response

    def _arun(self, URL: str):
        raise NotImplementedError("query_csv_file does not support async")

class CSVQueringTool(BaseTool):
    name = "query_csv_file_with_prompt"
    # description = """
    # Download a CSV document from a specified URL, then scan it for content matching the given prompt, displaying the findings in a json format.
    # """
    description = """
    This function scans the csv document with a defined search string to find specific data, and presents the results in JSON format for easy access and further use.
    """
    args_schema: Type[BaseModel] = PromptCSVInput

    def _run(self, prompt: str):
        response = query_csv_file_with_prompt(prompt)
        print("function response", response)
        return response

    def _arun(self, prompt: str):
        raise NotImplementedError("query_csv_file_with_prompt does not support async")


# Set up the turbo LLM
turbo_llm = ChatOpenAI(
    temperature=0,
    model_name='gpt-3.5-turbo-16k'
)
prompt ="""
        You are an assistant that helps to get access to CSV documents tools. You should always:
        - Take the user input, and list all the provided arguments within the user input.
        - Pass the arguments to the provided tools when being used
        - Be polite and helpful
        - First action that you must download CSV URL through CSVProcessingTool 
        - Second action pass the prompt or the query to CSVQueringTool 
        - Provide explanations for your actions
        User: {input}
        Assistant:
        """

tools = [CSVProcessingTool(), CSVQueringTool()]
# create our agent
agent = initialize_agent(
    tools=tools,
    llm=turbo_llm,
    prompt=prompt,
    agent=AgentType.STRUCTURED_CHAT_ZERO_SHOT_REACT_DESCRIPTION,
    verbose=True,
    max_iteration=1
)

agent_output = agent.run("from the following URL `https://docs.google.com/spreadsheets/d/11N4v9tI3lgUrvhOEUCd08yoaJ-TF431Kvu8L4y2ABQY/edit?usp=sharing`, please tell me how many rows are there?")
print(agent_output)

# agent.run("how many rows are there?")
# agent.run("how many people have stayed more than 3 years in the city?")
# agent.run("how many people have stayed more than 3 years in the city and are female?")
# agent.run("Are there more males or females?")
# agent.run("can you summarize the data?")

# answer = agent.run("give me first 50 rows of the data")
# print("===========answer", answer)
