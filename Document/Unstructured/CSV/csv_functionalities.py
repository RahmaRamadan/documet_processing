from langchain.agents import create_csv_agent
from langchain.llms import OpenAI
from dotenv import load_dotenv
import requests

load_dotenv()

def download_sheet_as_csv(link):
    file_id = link.split('/spreadsheets/d/')[1].split('/edit')[0]
    # The API endpoint
    url = 'http://localhost:3000/api-n/drive/download-file'
    # The data to be sent with the POST request
    params = {'fileId': file_id}

    try:
        # Make a POST request to the API
        response = requests.post(url, params=params)
        response_json = response.json()
        file_path = response_json.get("file_path")

        # Check if the request was successful
        if response.status_code == 200:
            print('File downloaded successfully')
            print(response)
            return file_path
            # Here you can handle the response content if needed
            # For example, to save the file:
            # with open('downloaded_file', 'wb') as f:
            #     f.write(response.content)
        else:
            print(f'Failed to download file. Status code: {response.status_code}')
            return None
    
    except requests.exceptions.RequestException as e:
        # Handle any errors that occur during the request
        print(f'An error occurred: {e}')
        
# def download_sheet_as_csv(link, destination):
    
#     # Extract the file ID from the link
#     file_id = link.split('/spreadsheets/d/')[1].split('/edit')[0]
    
#     # Create the direct download link for CSV format
#     direct_link = f"https://docs.google.com/spreadsheets/d/{file_id}/export?format=csv"

#     # Make the request and save the content in the specified destination
#     response = requests.get(direct_link, stream=True)
    
#     response.raise_for_status()
#     with open(destination, "wb") as f:
#         for chunk in response.iter_content(chunk_size=32768):
#             f.write(chunk)
            
local_path = ''
def query_csv_file(url):
    print(url)
    try:
        # out_csv_folder = "csvOutput"
        # file_name = "downloadedCSV.csv"
        # if not os.path.exists(out_csv_folder):
        #     os.makedirs(out_csv_folder)
        # local_path = os.path.abspath(f"{out_csv_folder}/{file_name}")
        # print("===============local path", local_path)
        local_path = download_sheet_as_csv(url)
        if local_path == None:
            return None 
        else:
        # df = pd.read_csv('/content/train.csv')
        # agent = create_csv_agent(OpenAI(temperature=0), 
        #                         local_path, 
        #                         verbose=True)     
        # answer = agent.run(query_prompt) 
            return local_path 
    except Exception as e:
        print("error", e)
                    
def query_csv_file_with_prompt(query_prompt):
    try:
        # out_csv_folder = "csvOutput"
        # file_name = "downloadedCSV.csv"
        # if not os.path.exists(out_csv_folder):
        #     os.makedirs(out_csv_folder)
        # local_path = os.path.abspath(f"{out_csv_folder}/{file_name}")
        
        agent = create_csv_agent(OpenAI(temperature=0), 
                                local_path, 
                                verbose=True)     
        answer = agent.run(query_prompt) 
        return answer 
    except Exception as e:
        print("error", e)