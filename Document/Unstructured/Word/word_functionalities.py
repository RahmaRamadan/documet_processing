from langchain.document_loaders import UnstructuredWordDocumentLoader
from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain.embeddings import OpenAIEmbeddings
from langchain.chat_models import ChatOpenAI
from langchain.prompts import PromptTemplate
from langchain.vectorstores import Chroma
from langchain.chains import LLMChain
from dotenv import load_dotenv
import requests

load_dotenv()

def download_google_doc_as_word(link):
    file_id = link.split('/d/')[1].split('/')[0]
    # The API endpoint
    url = 'http://localhost:3000/api-n/drive/download-file'
    # The data to be sent with the POST request
    params = {'fileId': file_id}

    try:
        # Make a POST request to the API
        response = requests.post(url, params=params)
        if response.status_code == 200:
            response_json = response.json()
            file_path = response_json.get("file_path")
            return file_path
            
        else:
            print(f'Failed to download file. Status code: {response.status_code}')
            return None 
    
    except requests.exceptions.RequestException as e:
        # Handle any errors that occur during the request
        print(f'An error occurred: {e}')
        
    
def word_file_searching(URL, query):
    """
    Searches for relevant content related to the provided query within a Word file and presents the results in json format.
    """
    # Load the Word file
    # Create a custom agent using the ChatOpenAI model
    llm = ChatOpenAI(model_name="gpt-3.5-turbo", temperature=0)
    loader = UnstructuredWordDocumentLoader(URL)
    document = loader.load()
    
    # Split the document into chunks
    text_splitter = RecursiveCharacterTextSplitter(chunk_size=500, chunk_overlap=0)

    chunks = text_splitter.split_documents(document)
    
    prompt = PromptTemplate.from_template(
        " make the retrieved docs im bullet points: {docs}"
    )
    # Chain
    llm_chain = LLMChain(llm=llm, prompt=prompt)

    # Embed and store the chunks
    embeddings = OpenAIEmbeddings()
    vectorstore = Chroma.from_documents(documents=chunks, embedding=embeddings)
  
    docs = vectorstore.similarity_search(query)
    print(len(docs))
    
    # Perform the query
    result = llm_chain(docs)
    return result["text"] 

def query_word_file(url, query_prompt):
    try:
       
        local_path = download_google_doc_as_word(url)
        if local_path == None:
            return None 
        else:
            response = word_file_searching(local_path, query_prompt)
            
            return response
    except Exception as e:
        print("error", e)