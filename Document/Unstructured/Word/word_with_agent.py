from word_functionalities import query_word_file
from langchain.agents import AgentType,initialize_agent
from langchain.chat_models import ChatOpenAI
from pydantic import BaseModel, Field
from langchain.tools import BaseTool
from dotenv import load_dotenv
from typing import Type


load_dotenv()

class QueryWordInput(BaseModel):
    """Inputs for query_word_file"""

    URL: str = Field(description="The web address or path where the Word document resides")
    query: str = Field(description="The search string or keyword for which relevant content from the Word file is to be retrieved")

class WordProcessingTool(BaseTool):
    name = "query_word_file"
    description = """
    Download a Word document from a specified URL, then scan it for content matching the given query, displaying the findings in a json format.
    """
    args_schema: Type[BaseModel] = QueryWordInput

    def _run(self, URL: str, query: str):
        response = query_word_file(URL, query)
        print("function response", response)
        return response

    def _arun(self, URL: str, query: str):
        raise NotImplementedError("query_word_file does not support async")

# Set up the turbo LLM
turbo_llm = ChatOpenAI(
    temperature=0,
    model_name='gpt-3.5-turbo'
)

tools = [WordProcessingTool()]
# create our agent
agent = initialize_agent(
    tools=tools,
    llm=turbo_llm,
    agent=AgentType.STRUCTURED_CHAT_ZERO_SHOT_REACT_DESCRIPTION,
    verbose=True,
    max_iterations=4,
)

agent_output = agent.run("from the following URL `https://docs.google.com/document/d/13WbJAxO2463rBLOtRoE3CELGvOkZdr7k/edit?usp=sharing&ouid=104825586842121347927&rtpof=true&sd=true`, please tell me only the internships?")
print(agent_output)