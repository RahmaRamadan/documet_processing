from langchain.document_loaders import UnstructuredPowerPointLoader
from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain.embeddings import OpenAIEmbeddings
from langchain.chat_models import ChatOpenAI
from langchain.prompts import PromptTemplate
from langchain.vectorstores import Chroma
from langchain.chains import LLMChain
from dotenv import load_dotenv
import requests

load_dotenv()

            
def download_powerpoint_file_from_google(link):
    file_id = link.split('/presentation/d/')[1].split('/edit')[0]
    # The API endpoint
    url = 'http://localhost:3000/api-n/drive/download-file'
    # The data to be sent with the POST request
    params = {'fileId': file_id}

    try:
        # Make a POST request to the API
        response = requests.post(url, params=params)
        if response.status_code == 200:
            response_json = response.json()
            file_path = response_json.get("file_path")
            return file_path
           
        else:
            print(f'Failed to download file. Status code: {response.status_code}')
            return None 
    
    except requests.exceptions.RequestException as e:
        # Handle any errors that occur during the request
        print(f'An error occurred: {e}')
        
             
def powerpoint_file_searching(file_path, query):
    # Load the Word file
    # Create a custom agent using the ChatOpenAI model
    llm = ChatOpenAI(model_name="gpt-3.5-turbo", temperature=0)

    loader = UnstructuredPowerPointLoader(file_path)
    document = loader.load()
    # Split the document into chunks
    text_splitter = RecursiveCharacterTextSplitter(chunk_size=500, chunk_overlap=0)

    chunks = text_splitter.split_documents(document)
    
    prompt = PromptTemplate.from_template(
        " make the retrieved docs im bullet points: {docs}"
    )
    # Chain
    llm_chain = LLMChain(llm=llm, prompt=prompt)

    # Embed and store the chunks
    embeddings = OpenAIEmbeddings()
    vectorstore = Chroma.from_documents(documents=chunks, embedding=embeddings)
  
    docs = vectorstore.similarity_search(query)
    print(len(docs))
    
    result = llm_chain(docs)
    return result["text"] 

def query_powerpoint_file(url, query_prompt):
    try:
        local_path = download_powerpoint_file_from_google(url)
        if local_path == None:
            return None 
        else:
            response = powerpoint_file_searching(local_path, query_prompt)
            
            return response
    except Exception as e:
        print("error", e)
        
        
# Example usage
# file_path = 'https://docs.google.com/presentation/d/1d3D1LgZ0kSgO-4a80ZC_Xy5ZL4HmRfby/edit?usp=sharing&ouid=104825586842121347927&rtpof=true&sd=true'
# query = 'what is the future of Artificial Intelligence?'

# result = query_powerpoint_file(file_path, query)
# print(result)