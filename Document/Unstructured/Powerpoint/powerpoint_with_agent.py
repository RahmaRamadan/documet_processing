from powerpoint_functionalities import query_powerpoint_file
from langchain.agents import AgentType,initialize_agent
from langchain.chat_models import ChatOpenAI
from pydantic import BaseModel, Field
from langchain.tools import BaseTool
from dotenv import load_dotenv
from typing import Type

load_dotenv()

class QueryPowerpointInput(BaseModel):
    """Inputs for query_powerpoint_file"""

    URL: str = Field(description="The web address or path where the Powerpoint document resides")
    query: str = Field(description="The search string or keyword for which relevant content from the Powerpoint file is to be retrieved")

class PowerpointProcessingTool(BaseTool):
    name = "query_powerpoint_file"
    description = """
    Download a Powerpoint document from a specified URL, then scan it for content matching the given query, displaying the findings in a json format.
    """
    args_schema: Type[BaseModel] = QueryPowerpointInput

    def _run(self, URL: str, query: str):
        response = query_powerpoint_file(URL, query)
        print("function response", response)
        return response

    def _arun(self, URL: str, query: str):
        raise NotImplementedError("query_powerpoint_file does not support async")

# Set up the turbo LLM
turbo_llm = ChatOpenAI(
    temperature=0,
    model_name='gpt-3.5-turbo'
)

tools = [PowerpointProcessingTool()]
# create our agent
agent = initialize_agent(
    tools=tools,
    llm=turbo_llm,
    agent=AgentType.STRUCTURED_CHAT_ZERO_SHOT_REACT_DESCRIPTION,
    verbose=True,
    max_iterations=3,
)

agent_output = agent.run("from the following URL `https://docs.google.com/presentation/d/1d3D1LgZ0kSgO-4a80ZC_Xy5ZL4HmRfby/edit?usp=sharing&ouid=104825586842121347927&rtpof=true&sd=true`, please tell me what is the future of Artificial Intelligence?")
print(agent_output)



