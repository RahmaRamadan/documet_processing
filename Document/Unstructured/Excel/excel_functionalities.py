from langchain_experimental.agents.agent_toolkits import create_pandas_dataframe_agent
from langchain.agents import  AgentType
from langchain.chat_models.openai import ChatOpenAI
from dotenv import load_dotenv
from pathlib import Path
from fpdf import FPDF
import pandas as pd
import requests
import os 
load_dotenv()


def download_sheet_as_excel(link):
    file_id = link.split('/spreadsheets/d/')[1].split('/edit')[0]
    # The API endpoint
    url = 'http://localhost:3000/api-n/drive/download-file'
    # The data to be sent with the POST request
    params = {'fileId': file_id}

    try:
        # Make a POST request to the API
        response = requests.post(url, params=params)
        response_json = response.json()
        file_path = response_json.get("file_path")

        # Check if the request was successful
        if response.status_code == 200:
            print('File downloaded successfully')
            print(response)
            return file_path
            # Here you can handle the response content if needed
            # For example, to save the file:
            # with open('downloaded_file', 'wb') as f:
            #     f.write(response.content)
        else:
            print(f'Failed to download file. Status code: {response.status_code}')
            return None 
    
    except requests.exceptions.RequestException as e:
        # Handle any errors that occur during the request
        print(f'An error occurred: {e}')

def create_pdf(pdf_path: str, title: str, content: str):
    """
    Create a PDF document with the specified title and content.

    Parameters:
    - filename (str): The name of the output PDF file.
    - title (str): The title that will appear at the top of the PDF document.
    - content (str): The main content/body of the PDF document.
    """
    folder_path = Path(pdf_path).parent

    # Define the new folder path for 'images'
    output_folder = folder_path / 'Unstructured_Result' 
    
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)
    name = Path(pdf_path).stem
    filename = f"{output_folder}/{name}.pdf"
    # Font, size, style, and alignment configurations
    title_font, title_size, title_style, title_align = "Arial", 16, 'B', 'C'
    content_font, content_size, content_style, content_align = "Arial", 12, '', 'L'
    
    # Page configurations
    page_orientation, page_format = 'P', 'A4'
    
    # Header and Footer configurations
    

    pdf = FPDF(orientation=page_orientation, format=page_format)
    pdf.add_page()

    # Header
    pdf.set_font(content_font, content_style, content_size)

    # Title
    pdf.set_font(title_font, title_style, title_size)
    pdf.cell(0, 10, txt=title, ln=True, align=title_align)

    # Content
    pdf.set_font(content_font, content_style, content_size)
    pdf.multi_cell(0, 10, txt=content, align=content_align)

    # Footer
    pdf.set_y(-15)
    pdf.set_font(content_font, content_style, content_size)

    pdf.output(filename)

def query_excel_file(url, query_prompt):
    try:

        local_path = download_sheet_as_excel(url)
        if local_path == None:
            return None 
        # print("=======local path", local_path)
        # Set the options to display all columns and rows
        # pd.set_option('display.max_columns', None)  # No limit on number of columns
        # pd.set_option('display.max_rows', None)     # No limit on number of rows
        # pd.set_option('display.width', None)        # Automatically detect the display width
        # pd.set_option('display.max_colwidth', None) # Display full width of each column  
        else:
            df = pd.read_excel(local_path)
            # print("=========df", df)
            agent = create_pandas_dataframe_agent(
                ChatOpenAI(temperature=0, model="gpt-3.5-turbo"),
                df,
                verbose=True,
                agent_type=AgentType.OPENAI_FUNCTIONS,
            )
            
            answer = agent.run(query_prompt) 
            create_pdf("result.pdf","result", answer)
            return answer 
    except Exception as e:
        print("error", e)
                    
     