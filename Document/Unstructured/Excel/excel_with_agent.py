from langchain.agents import AgentType,initialize_agent
from excel_functionalities import query_excel_file
from langchain.chat_models import ChatOpenAI
from pydantic import BaseModel, Field
from langchain.tools import BaseTool
from dotenv import load_dotenv
from typing import Type

load_dotenv()

# os.environ["OPENAI_API_KEY"] = "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"
class QueryExcelInput(BaseModel):
    """Inputs for query_excel_file"""

    URL: str = Field(description="The web address or path where the Excel document resides")
    prompt: str = Field(description="The provided string specifies a text-based search string, designed to retrieve relevant information or content based on keyword matches. This string is meant for a text processing prompt and is not related to database or SQL operations. Users should structure their input in a clear and concise manner to ensure optimal results.")

class ExcelProcessingTool(BaseTool):
    name = "query_excel_file"
    # description = """
    # Download a Excel document from a specified URL, then scan it for content matching the given prompt, displaying the findings in a json format.
    # """
    description = """
    This function downloads an Excel file from Google Drive, scans it with a defined search string to find specific data, 
    and presents the results in JSON format for easy access and further use.
    """
    args_schema: Type[BaseModel] = QueryExcelInput

    def _run(self, URL: str, prompt: str):
        print("========url", URL, prompt)
        response = query_excel_file(URL, prompt)
        print("function response", response)
        return response

    def _arun(self, URL: str, prompt: str):
        raise NotImplementedError("query_excel_file does not support async")

# Set up the turbo LLM
turbo_llm = ChatOpenAI(
    temperature=0,
    model_name='gpt-3.5-turbo'
)
prompt ="""
        You are an assistant that helps to get access to Excel documents tools. You should always:
        - Take the user input, and list all the provided arguments within the user input.
        - Pass the arguments to the provided tools as it is when being used
        - Be polite and helpful
        - Pass the Excel URL and pass the rest of the prompt or the query as a seperate argument to ExcelProcessingTool 
        - Provide explanations for your actions
        User: {input}
        Assistant:
        """
        
tools = [ExcelProcessingTool()]
# create our agent
agent = initialize_agent(
    tools=tools,
    llm=turbo_llm,
    agent=AgentType.OPENAI_MULTI_FUNCTIONS,
    verbose=True,
)

agent_output = agent.run("from the following URL `https://docs.google.com/spreadsheets/d/1-JrlVAxjNTGrXZVOkUohYgS0l-jpoqV5/edit?usp=sharing&ouid=104825586842121347927&rtpof=true&sd=true`, please give me first name and last name and age of id: 2579")
print(agent_output)
# output = agent.run("give me first name and last name and age of id: 6548")

